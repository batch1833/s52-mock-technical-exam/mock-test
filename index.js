function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
   if(letter.length == 1){
    for(let i = 0; i<sentence.length; i++){
        if(sentence[i] == letter){
            result++;
        }
    }

   }
    else{
        return undefined;
    }

    return result;
}


function isIsogram(text) {
    
    return text.split('').filter((item, pos, arr)=> arr.indexOf(item) == pos).length == text.length;
}


function purchase(age, price) {
    
    if (age < 13) {
        return undefined;
    }
    if ((age >= 13 && age <= 21) || age >= 65) {
        return String((price - ((price * 20)/100)).toFixed(2));
    } 
    if (age >= 22 && age <= 64) {
        return String(price.toFixed(2));
    }
}



module.exports = {
    countLetter,
    isIsogram,
    purchase
   
};